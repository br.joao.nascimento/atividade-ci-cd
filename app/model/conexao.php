<?php
$conexao;

$servername = "db";
$username = "root";
$password = "root";

try {
    //$conexao = new PDO('mysql:host=127.0.0.1:3306;dbname=atividade-ci-cd','root','123');
    $conexao = new PDO("mysql:host=$servername;dbname=atividade-ci-cd", $username, $password);
    // set the PDO error mode to exception
    $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully";
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

/*$conexao = new mysqli("db","root","123","atividade-ci-cd");

// Check connection
if ($conexao -> connect_errno) {
  echo "Failed to connect to MySQL: " . $conexao -> connect_error;
  exit();
}*/