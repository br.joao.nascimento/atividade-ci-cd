<?php
class Usuario
{
    private $id;
    private $nome;
    private $admin;

    /**
     * ...
     * getters e setters
     * ...
     *
     */

    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function isAdmin()
    {
        return $this->admin;
    }

    public function save()
    {
        // logica para salvar cliente no banco
    }

    public function update()
    {
        // logica para atualizar cliente no banco
    }

    public function remove()
    {
        // logica para remover cliente do banco
    }

    public function listAll()
    {
        $sql = "SELECT id, nome, admin
                FROM usuario
                ORDER BY id";

        include_once "conexao.php";
        $resultado = $conexao->query($sql);
        $lista = $resultado->fetchAll();

        //var_dump($lista);

        $listaUsuarios = array();

        foreach ($lista as $key => $item) {
            $usuario = new Usuario();
            $usuario->id = $item['id'];
            $usuario->nome = $item['nome'];
            $usuario->admin = $item['admin'];

            array_push($listaUsuarios, $usuario);
        }

        //var_dump($listaUsuarios);

        return $listaUsuarios;
    }

    /**
     * ...
     * outros métodos de abstração de banco
     * ...
     *
     */
}

?>