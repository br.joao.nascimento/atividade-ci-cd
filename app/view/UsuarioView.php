<?php
$usuarios = $_REQUEST['usuarios'];
?>;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Implementando MVC</title>

    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>

<body>
    <h1>ATIVIDADE CI-CD - JOÃO VICTOR NASCIMENTO DA SILVA</h1>
    
    <h3>Tabela de Usuarios</h3>
    <table>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Admin</th>
        </tr>
        <?php foreach ($usuarios as $usuario): ?>
            <tr>
                <td>
                    <?php echo $usuario->getId(); ?>
                </td>
                <td>
                    <?php echo $usuario->getNome(); ?>
                </td>
                <td>
                    <?php echo $usuario->isAdmin(); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>

</html>