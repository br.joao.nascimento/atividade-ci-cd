<?php
require_once 'model/Usuario.php';

class UsuarioController
{

    public function listar()
    {
        $usuario = new Usuario();
        $usuarios = $usuario->listAll();

        $_REQUEST['usuarios'] = $usuarios;

        require_once 'view/UsuarioView.php';
    }
}

?>